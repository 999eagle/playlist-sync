# Playlist sync

A tool to manage two versions of a music library: the high quality (FLAC) source and a lower quality (MP3) transcoded version. Playlists in m3u format are kept in sync as well. When transcoding, ReplayGain data can be calculated.

Inspired by [harmonize](https://github.com/nvllsvm/harmonize).

## Usage

```bash
# transcode from the high quality version in music/ to a lower quality version in music_transcoded/
# also replaygain data is calculated for files in both versions
playlist-sync -R album music/ music_transcoded/
```
