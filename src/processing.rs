use std::sync::atomic::{AtomicBool, Ordering};

use anyhow::{Context, Result};
use ebur128::EbuR128;
use rayon::prelude::*;

use crate::data::{
    music::ReplayGainData,
    tag::{Tag, TagKey},
    AlbumDirectory, AnyFile, MusicFile, OtherFile, OtherFileType, PlaylistFile,
};
use crate::util;

mod playlist;
mod transcode;

pub fn process_other_file(file: OtherFile) -> Result<()> {
    match file.file_type {
        OtherFileType::Dir => {
            std::fs::create_dir_all(&file.file_info.dst_path).with_context(|| {
                format!("Failed to create directory {:?}", file.file_info.dst_path)
            })?;
            util::copy_permissions(&file)?;
        }
        OtherFileType::File => {
            std::fs::copy(&file.file_info.src_path, &file.file_info.dst_path)
                .with_context(|| format!("Failed to copy file {:?}", file.file_info.src_path))?;
            util::copy_permissions(&file)?;
        }
    }

    Ok(())
}

pub fn process_album_dir(mut file: AlbumDirectory) -> Result<()> {
    log::info!("Checking album {:?}", file.file_info.rel_path);
    let calc_album_rg = file.file_info.config.replaygain == crate::config::ReplayGainMode::Album;
    let mut music_files: Vec<_> = if !calc_album_rg {
        // filter all ignored music files if not calculating album rg
        file.music_files
            .drain(..)
            .filter(|f| !f.should_ignore())
            .collect()
    } else {
        // use all files if any file is not ignored
        if file.music_files.iter().any(|f| !f.should_ignore()) {
            file.music_files.drain(..).collect()
        } else {
            Vec::new()
        }
    };

    if music_files.is_empty() {
        return Ok(());
    }

    // create album dir if it doesn't exist
    if !file.file_info.dst_path.exists() {
        std::fs::create_dir_all(&file.file_info.dst_path).with_context(|| {
            format!("Failed to create directory {:?}", &file.file_info.dst_path)
        })?;
        util::copy_permissions(&file)?;
    }

    // populate file metadata
    for file in music_files.iter_mut() {
        file.populate()
            .with_context(|| format!("Failed to read data for {:?}", file.file_info.rel_path))?;
    }

    // transcode individual files
    let success = AtomicBool::new(true);
    music_files.par_iter_mut().for_each(|file: &mut MusicFile| {
        if let Err(e) = self::transcode::transcode_file(file)
            .and_then(|_| self::transcode::calculate_destination_r128(file))
        {
            log::error!(
                "Error transcoding file {:?}: {:#}",
                file.file_info.src_path,
                e
            );
            let _ = std::fs::remove_file(&file.file_info.dst_path);
            success.store(false, Ordering::SeqCst);
        }
    });
    if !success.load(Ordering::SeqCst) {
        return Err(anyhow::anyhow!("Failed to transcode files"));
    }

    // calculate album replaygain data if requested
    if calc_album_rg {
        let (source_album_loudness, source_album_peak) =
            calculate_album_rg(music_files.iter().map(|file| {
                file.ext
                    .as_ref()
                    .map(|ext| ext.source_rg.as_ref())
                    .flatten()
            }))?;
        let (dest_album_loudness, dest_album_peak) = calculate_album_rg(
            music_files
                .iter()
                .map(|file| file.ext.as_ref().map(|ext| ext.dest_rg.as_ref()).flatten()),
        )?;
        for file in music_files.iter_mut() {
            let ext = file.ext.as_mut().unwrap();
            ext.source_tag.set_tag(
                TagKey::ReplayGainAlbumPeak,
                vec![ReplayGainData::format_peak(source_album_peak)],
            );
            ext.source_tag.set_tag(
                TagKey::ReplayGainAlbumGain,
                vec![ReplayGainData::format_gain(source_album_loudness)],
            );
            ext.dest_tag.set_tag(
                TagKey::ReplayGainAlbumPeak,
                vec![ReplayGainData::format_peak(dest_album_peak)],
            );
            ext.dest_tag.set_tag(
                TagKey::ReplayGainAlbumGain,
                vec![ReplayGainData::format_gain(dest_album_loudness)],
            );
        }
    }
    for file in music_files.iter_mut() {
        if let Some(ext) = file.ext.as_mut() {
            if file.file_info.config.strip_tags {
                ext.source_tag.remove_unknown_tags();
            }
        }
        if let Err(e) = self::transcode::copy_music_tags(file) {
            log::error!(
                "Error copying file tags for {:?}: {:#}",
                file.file_info.rel_path,
                e
            );
        }
        let ext = file.ext.as_mut().unwrap();
        if let Err(e) = ext.source_tag.write_to_path(&file.file_info.src_path) {
            log::error!(
                "Failed writing tags to {:?}: {:#}",
                file.file_info.src_path,
                e
            );
        }
        if let Err(e) = ext.dest_tag.write_to_path(&file.file_info.dst_path) {
            log::error!(
                "Failed writing tags to {:?}: {:#}",
                file.file_info.dst_path,
                e
            );
        }
        if let Err(e) = util::copy_permissions(file) {
            log::warn!("{:#}", e);
        }
    }

    Ok(())
}

pub fn process_playlist_file(mut file: PlaylistFile) -> Result<()> {
    log::info!("Checking playlist {:?}", file.file_info.rel_path);

    file.populate()?;
    self::playlist::write(&file)?;

    Ok(())
}

fn calculate_album_rg<'r, I: Iterator<Item = Option<&'r ReplayGainData>>>(
    rg_data: I,
) -> Result<(f64, f64)> {
    let mut album_peak = 0.0_f64;
    let mut album_r128: Vec<&EbuR128> = Vec::new();

    for rg in rg_data {
        if let Some(rg) = rg {
            album_peak = album_peak.max(rg.peak);
            album_r128.push(&rg.r128);
        } else {
            return Err(anyhow::anyhow!(
                "Failed to calculate album loudness: replay gain data missing"
            ));
        }
    }

    match EbuR128::loudness_global_multiple(album_r128.into_iter()) {
        Ok(loudness) => {
            log::debug!(
                "Calculated ReplayGain data for album: loudness: {} LUFS, peak: {}",
                loudness,
                album_peak
            );

            Ok((loudness, album_peak))
        }
        Err(e) => {
            return Err(anyhow::anyhow!("Failed to calculate album loudness: {}", e));
        }
    }
}
