use bin_common::{CrateSetup, CrateSetupBuilder};
use clap::{App, Arg, ArgMatches};
use relative_path::{RelativePath, RelativePathBuf};
use std::path::PathBuf;
use std::time::SystemTime;
use version::version;

#[derive(Debug, Clone)]
pub struct Config {
    pub base_path: PathBuf,
    pub flac_root: PathBuf,
    pub flac_root_rel: RelativePathBuf,
    pub mp3_root: PathBuf,
    pub mp3_root_rel: RelativePathBuf,
    pub strip_roots: Vec<PathBuf>,
    pub force: bool,
    pub remove_missing: bool,
    pub percent_decode: bool,
    pub strip_tags: bool,
    pub remove_destination_files: bool,
    pub replaygain: ReplayGainMode,
    pub target_bitrate: i32,
    pub last_run: SystemTime,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ReplayGainMode {
    None,
    Track,
    Album,
}

impl Config {
    pub fn read_from_arg_matches(matches: &ArgMatches) -> Self {
        let flac_root = PathBuf::from(matches.value_of("FLAC path").unwrap());
        let flac_root_rel = RelativePath::new(matches.value_of("FLAC path").unwrap()).to_owned();
        let mp3_root = PathBuf::from(matches.value_of("MP3 path").unwrap());
        let mp3_root_rel = RelativePath::new(matches.value_of("MP3 path").unwrap()).to_owned();
        let mut strip_roots = matches
            .values_of("strip-root")
            .map_or(Vec::new(), |values| {
                values.into_iter().map(PathBuf::from).collect()
            });
        strip_roots.push(flac_root.clone());
        strip_roots.push(mp3_root.clone());
        let force = matches.is_present("force");
        let remove_missing = matches.is_present("clean-playlist");
        let percent_decode = matches.is_present("percent-decode");
        let strip_tags = matches.is_present("strip-tags");
        let remove_destination_files = matches.is_present("rm");
        let replaygain = match matches.value_of("replaygain") {
            Some("none") => ReplayGainMode::None,
            Some("track") => ReplayGainMode::Track,
            Some("album") => ReplayGainMode::Album,
            _ => {
                log::error!("BUG: cmdline arg for replaygain invalid");
                ReplayGainMode::None
            }
        };
        let target_bitrate = matches
            .value_of("target-bitrate")
            .expect("BUG: cmdline arg for bitrate missing")
            .parse::<i32>()
            .expect("BUG: cmdline arg validator for bitrate failed");

        let base_path = std::env::current_dir().unwrap_or_else(|_| PathBuf::from("."));
        let last_run = crate::util::get_last_run_time(&base_path).unwrap_or_else(|e| {
            log::warn!("failed to get last run time: {}", e);
            SystemTime::UNIX_EPOCH
        });

        Self {
            base_path,
            flac_root,
            flac_root_rel,
            mp3_root,
            mp3_root_rel,
            force,
            remove_missing,
            strip_roots,
            percent_decode,
            strip_tags,
            remove_destination_files,
            replaygain,
            target_bitrate,
            last_run,
        }
    }
}

pub fn get_crate_setup() -> CrateSetup {
    CrateSetupBuilder::new()
        .with_app_name("playlist-sync")
        .build()
        .expect("CrateSetup")
}

pub fn get_app(crate_setup: &CrateSetup) -> App {
    App::new(crate_setup.application_name())
        .version(version!())
        .about("A tool to keep a high quality and a lower quality version of the same music library in sync.")
        .arg(
            Arg::with_name("v")
                .short("v")
                .long("verbose")
                .multiple(true)
                .help("Increase verbosity. May be specified multiple times."),
        )
        .arg(
            Arg::with_name("q")
                .short("q")
                .long("quiet")
                .multiple(true)
                .help("Decrease verbosity. May be specified multiple times."),
        )
        .arg(
            Arg::with_name("force")
                .short("f")
                .long("force")
                .help("Force processing all files instead of only changed files."),
        )
        .arg(
            Arg::with_name("strip-root")
                .long("strip-root")
                .help("Root path to strip from playlist entries. May be specified multiple times.")
                .multiple(true)
                .number_of_values(1)
                .value_name("ROOT"),
        )
        .arg(
            Arg::with_name("clean-playlist")
                .long("clean-playlist")
                .short("c")
                .help("Remove playlist entries with missing files."),
        )
        .arg(
            Arg::with_name("percent-decode")
                .long("percent-decode")
                .short("d")
                .help("Decode percent encoded entries in playlists."),
        )
        .arg(
            Arg::with_name("strip-tags")
                .long("strip-tags")
                .short("s")
                .help("Remove unknown tags from input files."),
        )
        .arg(
            Arg::with_name("rm")
                .long("remove-destination")
                .short("r")
                .help("Remove existing destination files without a corresponding source file."),
        )
        .arg(
            Arg::with_name("replaygain")
                .long("replaygain-mode")
                .short("R")
                .default_value("none")
                .possible_values(&["none", "track", "album"])
                .help("Specifiy whether ReplayGain data should be calculated and if only per track or for entire albums as well.")
        )
        .arg(
            Arg::with_name("target-bitrate")
                .long("bitrate")
                .short("b")
                .default_value("320")
                .validator(|bitrate| {
                    match bitrate.parse::<i32>() {
                        Ok(_) => Ok(()),
                        Err(e) => Err(format!("{}", e)),
                    }
                })
                .help("Target bitrate in kbps of transcoded mp3 files.")
        )
        .arg(
            Arg::with_name("FLAC path")
                .help("Path to the high quality source library.")
                .required(true)
                .index(1)
        )
        .arg(
            Arg::with_name("MP3 path")
                .help("Path to the lower quality target library.")
                .required(true)
                .index(2)
        )
}
