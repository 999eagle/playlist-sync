use super::{
    tag::{Picture, PictureType, SourceTag, Tag, TagKey},
    MusicFile,
};
use anyhow::{Context, Result};
use ebur128::EbuR128;
use std::path::Path;

pub const REPLAYGAIN_TARGET_LUFS: f64 = -18.0;

pub struct MusicFileExt {
    pub source_rg: Option<ReplayGainData>,
    pub dest_rg: Option<ReplayGainData>,
    pub source_tag: FileTag,
    pub dest_tag: FileTag,
}

#[derive(Debug)]
pub struct ReplayGainData {
    pub(crate) r128: EbuR128,
    pub(crate) peak: f64,
    pub(crate) loudness: f64,
}

#[derive(Clone)]
pub struct FileTag {
    source_tag: SourceTag,
    changed: bool,
}

impl MusicFileExt {
    pub fn populate(file: &mut MusicFile) -> Result<()> {
        let src_tag = FileTag::from_path(&file.file_info.src_path)?;
        let dst_tag =
            FileTag::from_path(&file.file_info.dst_path).unwrap_or_else(|_| FileTag::new_id3());
        file.ext.replace(Box::new(Self {
            source_rg: None,
            dest_rg: None,
            source_tag: src_tag,
            dest_tag: dst_tag,
        }));
        Ok(())
    }
}

impl ReplayGainData {
    pub fn from_r128(r128: EbuR128) -> Result<Self> {
        let loudness = r128.loudness_global()?;
        let mut peak: f64 = 0.0;
        for c in 0..r128.channels() {
            peak = peak.max(r128.sample_peak(c)?);
        }
        Ok(Self {
            r128,
            peak,
            loudness,
        })
    }

    pub fn as_gain(&self) -> String {
        Self::format_gain(self.loudness)
    }

    pub fn as_peak(&self) -> String {
        Self::format_peak(self.peak)
    }

    pub fn format_gain(loudness: f64) -> String {
        format!("{:.2} dB", REPLAYGAIN_TARGET_LUFS - loudness)
    }

    pub fn format_peak(peak: f64) -> String {
        format!("{:.6}", peak)
    }
}

impl FileTag {
    fn from_path(path: &Path) -> Result<Self> {
        let source_tag = match path.extension().map(|e| e.to_str()).flatten() {
            Some("flac") => {
                let tag = metaflac::Tag::read_from_path(path)
                    .with_context(|| format!("Failed reading metadata tags from {:?}", path))?;
                SourceTag::FLAC(tag)
            }
            Some("mp3") => {
                let tag = id3::Tag::read_from_path(path)
                    .with_context(|| format!("Failed reading metadata tags from {:?}", path))?;
                SourceTag::MP3(tag)
            }
            Some(_) | None => return Err(anyhow::anyhow!("Unsupported file")),
        };
        Ok(Self {
            source_tag,
            changed: false,
        })
    }

    fn new_id3() -> Self {
        Self {
            source_tag: SourceTag::MP3(id3::Tag::new()),
            changed: false,
        }
    }

    pub fn write_to_path(&mut self, path: &Path) -> Result<()> {
        if self.changed {
            match &mut self.source_tag {
                SourceTag::FLAC(tag) => tag.write_to_path(path)?,
                SourceTag::MP3(tag) => tag.write_to_path(path, id3::Version::Id3v24)?,
            }
        }
        Ok(())
    }
}

impl Tag for FileTag {
    fn get_tag(&self, key: TagKey) -> Option<Vec<String>> {
        self.source_tag.get_tag(key)
    }

    fn set_tag(&mut self, key: TagKey, values: Vec<String>) {
        if self
            .source_tag
            .get_tag(key)
            .map(|v| v != values)
            .unwrap_or(true)
        {
            self.source_tag.set_tag(key, values);
            self.changed = true;
        }
    }

    fn remove_tag(&mut self, key: TagKey) {
        self.source_tag.remove_tag(key);
        self.changed = true;
    }

    fn remove_unknown_tags(&mut self) -> bool {
        let changed = self.source_tag.remove_unknown_tags();
        self.changed = self.changed || changed;
        changed
    }

    fn get_picture(&self, pic_type: PictureType) -> Option<Picture> {
        self.source_tag.get_picture(pic_type)
    }

    fn set_picture(&mut self, pic: Picture) {
        if self
            .source_tag
            .get_picture(pic.type_())
            .map(|p| p != pic)
            .unwrap_or(true)
        {
            self.source_tag.set_picture(pic);
            self.changed = true;
        }
    }

    fn remove_picture(&mut self, pic_type: PictureType) {
        self.source_tag.remove_picture(pic_type);
        self.changed = true;
    }
}
