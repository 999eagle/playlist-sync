use super::PlaylistFile;
use crate::config::Config;
use anyhow::{Context, Result};
use m3u::{Entry, EntryExt};
use percent_encoding::percent_decode_str;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::time::SystemTime;

pub struct PlaylistFileExt {
    pub entries: Vec<EntryExt>,
    pub dst_mtime: SystemTime,
}

impl PlaylistFileExt {
    pub fn populate(file: &mut PlaylistFile) -> Result<()> {
        let dst_mtime =
            crate::util::get_mtime(&file.file_info.dst_path).unwrap_or(SystemTime::UNIX_EPOCH);

        let read_flac = !ignore_playlist_file(
            &file.file_info.src_path,
            file.file_info.mtime,
            file.file_info.config,
        ) || !file.file_info.dst_path.exists();
        let read_mp3 =
            !ignore_playlist_file(&file.file_info.dst_path, dst_mtime, file.file_info.config);

        if !read_flac && !read_mp3 {
            log::debug!("Nothing to do for playlist {:?}", file.file_info.rel_path);
            return Ok(());
        }

        let mut entries = read(
            if read_flac {
                &file.file_info.src_path
            } else {
                &file.file_info.dst_path
            },
            file.file_info.config,
        )?;

        if read_flac && read_mp3 {
            log::debug!("Merging playlist");
            let other = read(&file.file_info.dst_path, file.file_info.config)?;
            merge_playlist(&mut entries, other);
        }

        file.ext.replace(Self { entries, dst_mtime });

        Ok(())
    }
}

fn read(path: &Path, config: &Config) -> Result<Vec<EntryExt>> {
    let mut reader = m3u::Reader::open_ext(path)
        .with_context(|| format!("Failed to read playlist file {:?}", path))?;
    let mut entries = Vec::new();
    for entry in reader.entry_exts() {
        let mut entry = entry.context("Failed to decode entry")?;
        process_entry(&mut entry.entry, config);
        entries.push(entry);
    }

    Ok(entries)
}

fn process_entry(entry: &mut Entry, config: &Config) {
    if let Entry::Path(path) = entry {
        log::trace!("Checking entry {:?}", path);
        for root in &config.strip_roots {
            if path.starts_with(root) {
                log::trace!("Stripping root {:?} from entry", root);
                *path = path.strip_prefix(root).unwrap().to_path_buf();
            }
        }
        if config.percent_decode {
            if let Some(path_str) = path.to_str() {
                if path_str.contains('%') {
                    log::trace!("Percent decoding path");
                    match PathBuf::from_str(&percent_decode_str(path_str).decode_utf8_lossy()) {
                        Ok(p) => *path = p,
                        Err(e) => {
                            log::warn!("Failed to percent decode path: {}", e);
                        }
                    }
                }
            } else {
                log::warn!("Failed to get path string from {:?}", path);
            }
        }
    }
}

fn merge_playlist(entries: &mut Vec<EntryExt>, other: Vec<EntryExt>) {
    for entry in other {
        if let Entry::Path(path) = &entry.entry {
            let mut new_track = true;
            for e in entries.iter_mut() {
                if let Entry::Path(p) = &e.entry {
                    if path.file_stem() == p.file_stem() && path.parent() == p.parent() {
                        new_track = false;
                        break;
                    }
                }
            }
            if new_track {
                log::trace!("Adding new entry {:?}", path);
                entries.push(entry.clone());
            }
        }
    }
}

fn ignore_playlist_file(path: &Path, mtime: SystemTime, config: &Config) -> bool {
    if !path.exists() || mtime <= config.last_run {
        return true;
    }
    false
}
