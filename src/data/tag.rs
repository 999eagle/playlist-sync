use std::convert::{TryFrom, TryInto};

pub const ALL_TAG_KEYS: &[TagKey] = &[
    TagKey::Album,
    TagKey::AlbumArtist,
    TagKey::Artist,
    TagKey::Date,
    TagKey::Disc,
    TagKey::Genre,
    TagKey::Label,
    TagKey::Media,
    TagKey::MusicBrainzAlbumArtistID,
    TagKey::MusicBrainzAlbumID,
    TagKey::MusicBrainzAlbumReleaseCountry,
    TagKey::MusicBrainzAlbumStatus,
    TagKey::MusicBrainzAlbumType,
    TagKey::MusicBrainzArtistID,
    TagKey::MusicBrainzReleaseGroupID,
    TagKey::MusicBrainzReleaseTrackID,
    TagKey::MusicBrainzTrackID,
    TagKey::OriginalDate,
    TagKey::OriginalYear,
    TagKey::ReleaseCountry,
    TagKey::ReleaseStatus,
    TagKey::ReleaseType,
    TagKey::ReplayGainTrackGain,
    TagKey::ReplayGainTrackPeak,
    TagKey::ReplayGainAlbumGain,
    TagKey::ReplayGainAlbumPeak,
    TagKey::Title,
    TagKey::TotalTracks,
    TagKey::TotalDiscs,
    TagKey::Track,
    TagKey::Year,
];
pub const NON_COPY_TAG_KEYS: &[TagKey] = &[
    TagKey::ReplayGainTrackGain,
    TagKey::ReplayGainTrackPeak,
    TagKey::ReplayGainAlbumGain,
    TagKey::ReplayGainAlbumPeak,
];
pub const ALL_PICTURE_TYPES: &[PictureType] = &[
    PictureType::Other,
    PictureType::Icon,
    PictureType::OtherIcon,
    PictureType::CoverFront,
    PictureType::CoverBack,
    PictureType::Leaflet,
    PictureType::Media,
    PictureType::LeadArtist,
    PictureType::Artist,
    PictureType::Conductor,
    PictureType::Band,
    PictureType::Composer,
    PictureType::Lyricist,
    PictureType::RecordingLocation,
    PictureType::DuringRecording,
    PictureType::DuringPerformance,
    PictureType::ScreenCapture,
    PictureType::BrightFish,
    PictureType::Illustration,
    PictureType::BandLogo,
    PictureType::PublisherLogo,
];

#[derive(Clone)]
pub enum SourceTag {
    FLAC(metaflac::Tag),
    MP3(id3::Tag),
}

pub trait Tag {
    fn get_tag(&self, key: TagKey) -> Option<Vec<String>>;
    fn set_tag(&mut self, key: TagKey, values: Vec<String>);
    fn remove_tag(&mut self, key: TagKey);
    fn remove_unknown_tags(&mut self) -> bool;
    fn get_picture(&self, pic_type: PictureType) -> Option<Picture>;
    fn set_picture(&mut self, pic: Picture);
    fn remove_picture(&mut self, pic_type: PictureType);
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct Picture {
    picture_type: PictureType,
    mime_type: MimeType,
    description: String,
    data: Vec<u8>,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum PictureType {
    Other,
    Icon,
    OtherIcon,
    CoverFront,
    CoverBack,
    Leaflet,
    Media,
    LeadArtist,
    Artist,
    Conductor,
    Band,
    Composer,
    Lyricist,
    RecordingLocation,
    DuringRecording,
    DuringPerformance,
    ScreenCapture,
    BrightFish,
    Illustration,
    BandLogo,
    PublisherLogo,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum MimeType {
    Jpeg,
    Png,
    Bmp,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TagKey {
    Album,
    AlbumArtist,
    Artist,
    Date,
    Disc,
    Genre,
    Label,
    Media,
    MusicBrainzAlbumArtistID,
    MusicBrainzAlbumID,
    MusicBrainzAlbumReleaseCountry,
    MusicBrainzAlbumStatus,
    MusicBrainzAlbumType,
    MusicBrainzArtistID,
    MusicBrainzReleaseGroupID,
    MusicBrainzReleaseTrackID,
    MusicBrainzTrackID,
    OriginalDate,
    OriginalYear,
    ReleaseCountry,
    ReleaseStatus,
    ReleaseType,
    ReplayGainTrackGain,
    ReplayGainTrackPeak,
    ReplayGainAlbumGain,
    ReplayGainAlbumPeak,
    Title,
    TotalTracks,
    TotalDiscs,
    Track,
    Year,
}

impl Picture {
    pub fn type_(&self) -> PictureType {
        self.picture_type
    }
}

impl Tag for SourceTag {
    fn get_tag(&self, key: TagKey) -> Option<Vec<String>> {
        match self {
            Self::FLAC(tag) => tag.get_tag(key),
            Self::MP3(tag) => tag.get_tag(key),
        }
    }

    fn set_tag(&mut self, key: TagKey, values: Vec<String>) {
        match self {
            Self::FLAC(tag) => tag.set_tag(key, values),
            Self::MP3(tag) => tag.set_tag(key, values),
        }
    }

    fn remove_tag(&mut self, key: TagKey) {
        match self {
            Self::FLAC(tag) => tag.remove_tag(key),
            Self::MP3(tag) => tag.remove_tag(key),
        }
    }

    fn remove_unknown_tags(&mut self) -> bool {
        match self {
            Self::FLAC(tag) => tag.remove_unknown_tags(),
            Self::MP3(tag) => tag.remove_unknown_tags(),
        }
    }

    fn get_picture(&self, pic_type: PictureType) -> Option<Picture> {
        match self {
            Self::FLAC(tag) => tag.get_picture(pic_type),
            Self::MP3(tag) => tag.get_picture(pic_type),
        }
    }

    fn set_picture(&mut self, pic: Picture) {
        match self {
            Self::FLAC(tag) => tag.set_picture(pic),
            Self::MP3(tag) => tag.set_picture(pic),
        }
    }

    fn remove_picture(&mut self, pic_type: PictureType) {
        match self {
            Self::FLAC(tag) => tag.remove_picture(pic_type),
            Self::MP3(tag) => tag.remove_picture(pic_type),
        }
    }
}

impl Tag for metaflac::Tag {
    fn get_tag(&self, key: TagKey) -> Option<Vec<String>> {
        self.vorbis_comments()
            .map(|comment| comment.get(key.get_vorbis_key()))
            .flatten()
            .cloned()
    }

    fn set_tag(&mut self, key: TagKey, values: Vec<String>) {
        self.vorbis_comments_mut().set(key.get_vorbis_key(), values)
    }

    fn remove_tag(&mut self, key: TagKey) {
        self.vorbis_comments_mut().remove(key.get_vorbis_key())
    }

    fn remove_unknown_tags(&mut self) -> bool {
        let vorbis_comment = self.vorbis_comments_mut();
        let comments = &mut vorbis_comment.comments;
        let keys = comments.keys().cloned().collect::<Vec<_>>();
        let mut changed = false;
        for key in keys {
            if ALL_TAG_KEYS
                .iter()
                .map(|k| k.get_vorbis_key())
                .all(|k| k != key)
            {
                comments.remove(&key);
                changed = true;
            }
        }
        changed
    }

    fn get_picture(&self, pic_type: PictureType) -> Option<Picture> {
        let target_type = pic_type.into();
        self.pictures().find_map(|pic| {
            if pic.picture_type == target_type {
                match pic.clone().try_into() {
                    Ok(pic) => Some(pic),
                    Err(e) => {
                        log::warn!("{}", e);
                        None
                    }
                }
            } else {
                None
            }
        })
    }

    fn set_picture(&mut self, pic: Picture) {
        self.remove_picture_type(pic.picture_type.into());
        self.push_block(metaflac::block::Block::Picture(pic.into()));
    }

    fn remove_picture(&mut self, pic_type: PictureType) {
        self.remove_picture_type(pic_type.into());
    }
}

impl Tag for id3::Tag {
    fn get_tag(&self, key: TagKey) -> Option<Vec<String>> {
        let key_str = key.get_id3_key();
        if key_str.is_empty() {
            return None;
        }
        let value = if key_str == "TXXX" {
            let txxx_desc = key.get_id3_txxx_desc();
            self.frames().find_map(|frame| {
                if frame.id() != key_str {
                    None
                } else if let Some(content) = frame.content().extended_text() {
                    if content.description == txxx_desc {
                        Some(content.value.as_str())
                    } else {
                        None
                    }
                } else {
                    None
                }
            })
        } else {
            self.get(key_str)
                .map(|frame| frame.content().text())
                .flatten()
        };
        value
            .and_then(|value| key.process_read_id3_value(value))
            .map(|value| key.split_id3_value(value))
    }

    fn set_tag(&mut self, key: TagKey, values: Vec<String>) {
        let value = values.join("\0");
        let value = key.process_id3_value_to_write(value, self);
        let key_str = key.get_id3_key();
        if key_str.is_empty() {
            return;
        }
        let content = if key_str == "TXXX" {
            let txxx_desc = key.get_id3_txxx_desc();
            id3::Content::ExtendedText(id3::frame::ExtendedText {
                description: txxx_desc.into(),
                value,
            })
        } else {
            id3::Content::Text(value)
        };
        let mut frame = id3::Frame::with_content(key_str, content);
        frame.set_file_alter_preservation(key.file_alter_preservation());
        self.add_frame(frame);
    }

    fn remove_tag(&mut self, key: TagKey) {
        let key_str = key.get_id3_key();
        if key_str == "TXXX" {
            self.remove_extended_text(Some(key.get_id3_txxx_desc()), None)
        } else {
            self.remove(key_str)
        }
    }

    fn remove_unknown_tags(&mut self) -> bool {
        let mut changed = false;
        if self.frames().any(|f| f.id() == "PRIV") {
            changed = true;
        }
        self.remove("PRIV");
        let frames = self.frames().cloned().collect::<Vec<_>>();
        for frame in frames {
            match frame.id() {
                "TXXX" => {
                    let content = frame.content().extended_text().unwrap();
                    if ALL_TAG_KEYS
                        .iter()
                        .map(|k| k.get_id3_txxx_desc())
                        .all(|k| k != content.description)
                    {
                        self.remove_extended_text(Some(&content.description), Some(&content.value));
                        changed = true;
                    }
                }
                id if id.starts_with('T') => {
                    if ALL_TAG_KEYS
                        .iter()
                        .map(|k| k.get_id3_key())
                        .all(|key| key != id)
                    {
                        self.remove(id);
                        changed = true;
                    }
                }
                _ => (),
            }
        }
        changed
    }

    fn get_picture(&self, pic_type: PictureType) -> Option<Picture> {
        let target_type = pic_type.into();
        self.pictures().find_map(|pic| {
            if pic.picture_type == target_type {
                match pic.clone().try_into() {
                    Ok(pic) => Some(pic),
                    Err(e) => {
                        log::warn!("{}", e);
                        None
                    }
                }
            } else {
                None
            }
        })
    }

    fn set_picture(&mut self, pic: Picture) {
        self.add_picture(pic.into())
    }

    fn remove_picture(&mut self, pic_type: PictureType) {
        self.remove_picture_by_type(pic_type.into())
    }
}

impl TagKey {
    pub fn get_vorbis_key(&self) -> &'static str {
        match self {
            Self::Album => "ALBUM",
            Self::AlbumArtist => "ALBUMARTIST",
            Self::Artist => "ARTIST",
            Self::Date => "DATE",
            Self::Disc => "DISCNUMBER",
            Self::Genre => "GENRE",
            Self::Label => "LABEL",
            Self::Media => "MEDIA",
            Self::MusicBrainzAlbumArtistID => "MUSICBRAINZ_ALBUMARTISTID",
            Self::MusicBrainzAlbumID => "MUSICBRAINZ_ALBUMID",
            Self::MusicBrainzAlbumReleaseCountry => "MUSICBRAINZ_ALBUMRELEASECOUNTRY",
            Self::MusicBrainzAlbumStatus => "MUSICBRAINZ_ALBUMSTATUS",
            Self::MusicBrainzAlbumType => "MUSICBRAINZ_ALBUMTYPE",
            Self::MusicBrainzArtistID => "MUSICBRAINZ_ARTISTID",
            Self::MusicBrainzReleaseGroupID => "MUSICBRAINZ_RELEASEGROUPID",
            Self::MusicBrainzReleaseTrackID => "MUSICBRAINZ_RELEASETRACKID",
            Self::MusicBrainzTrackID => "MUSICBRAINZ_TRACKID",
            Self::OriginalDate => "ORIGINALDATE",
            Self::OriginalYear => "ORIGINALYEAR",
            Self::ReleaseCountry => "RELEASECOUNTRY",
            Self::ReleaseStatus => "RELEASESTATUS",
            Self::ReleaseType => "RELEASETYPE",
            Self::ReplayGainAlbumGain => "REPLAYGAIN_ALBUM_GAIN",
            Self::ReplayGainAlbumPeak => "REPLAYGAIN_ALBUM_PEAK",
            Self::ReplayGainTrackGain => "REPLAYGAIN_TRACK_GAIN",
            Self::ReplayGainTrackPeak => "REPLAYGAIN_TRACK_PEAK",
            Self::Title => "TITLE",
            Self::TotalDiscs => "DISCTOTAL",
            Self::TotalTracks => "TRACKTOTAL",
            Self::Track => "TRACKNUMBER",
            Self::Year => "ORIGINALYEAR",
        }
    }

    pub fn get_id3_key(&self) -> &'static str {
        match self {
            Self::Album => "TALB",
            Self::AlbumArtist => "TPE2",
            Self::Artist => "TPE1",
            Self::Date => "TDRC",
            Self::Disc => "TPOS",
            Self::Genre => "TCON",
            Self::Label => "TPUB",
            Self::Media => "TMED",
            Self::MusicBrainzAlbumArtistID => "TXXX",
            Self::MusicBrainzAlbumID => "TXXX",
            Self::MusicBrainzAlbumReleaseCountry => "TXXX",
            Self::MusicBrainzAlbumStatus => "TXXX",
            Self::MusicBrainzAlbumType => "TXXX",
            Self::MusicBrainzArtistID => "TXXX",
            Self::MusicBrainzReleaseGroupID => "TXXX",
            Self::MusicBrainzReleaseTrackID => "TXXX",
            Self::MusicBrainzTrackID => "TXXX",
            Self::OriginalDate => "TDOR",
            Self::OriginalYear => "",
            Self::ReleaseCountry => "",
            Self::ReleaseStatus => "",
            Self::ReleaseType => "",
            Self::ReplayGainAlbumGain => "TXXX",
            Self::ReplayGainAlbumPeak => "TXXX",
            Self::ReplayGainTrackGain => "TXXX",
            Self::ReplayGainTrackPeak => "TXXX",
            Self::Title => "TIT2",
            Self::TotalDiscs => "TPOS",
            Self::TotalTracks => "TRCK",
            Self::Track => "TRCK",
            Self::Year => "TYER",
        }
    }

    pub fn get_id3_txxx_desc(&self) -> &'static str {
        match self {
            Self::MusicBrainzAlbumArtistID => "MusicBrainz Album Artist Id",
            Self::MusicBrainzAlbumID => "MusicBrainz Album Id",
            Self::MusicBrainzAlbumReleaseCountry => "MusicBrainz Album Release Country",
            Self::MusicBrainzAlbumStatus => "MusicBrainz Album Status",
            Self::MusicBrainzAlbumType => "MusicBrainz Album Type",
            Self::MusicBrainzArtistID => "MusicBrainz Artist Id",
            Self::MusicBrainzReleaseGroupID => "MusicBrainz Release Group Id",
            Self::MusicBrainzReleaseTrackID => "MusicBrainz Release Track Id",
            Self::MusicBrainzTrackID => "MusicBrainz Track Id",
            Self::ReplayGainAlbumGain => "REPLAYGAIN_ALBUM_GAIN",
            Self::ReplayGainAlbumPeak => "REPLAYGAIN_ALBUM_PEAK",
            Self::ReplayGainTrackGain => "REPLAYGAIN_TRACK_GAIN",
            Self::ReplayGainTrackPeak => "REPLAYGAIN_TRACK_PEAK",
            _ => "",
        }
    }

    pub fn process_read_id3_value<'a>(&self, value: &'a str) -> Option<&'a str> {
        match self {
            Self::Track => Some(split_id3_value(value).0),
            Self::Disc => Some(split_id3_value(value).0),
            Self::TotalTracks => split_id3_value(value).1,
            Self::TotalDiscs => split_id3_value(value).1,
            _ => Some(value),
        }
    }

    pub fn split_id3_value(&self, value: &str) -> Vec<String> {
        if !matches!(self, Self::AlbumArtist | Self::Artist | Self::Genre) {
            return vec![value.to_owned()];
        }
        if value.contains('\0') {
            value.split('\0').map(|v| v.to_owned()).collect()
        } else {
            value.split(&[';'][..]).map(|v| v.to_owned()).collect()
        }
    }

    pub fn process_id3_value_to_write(&self, value: String, tag: &id3::Tag) -> String {
        match self {
            Self::Track => {
                if let Some(total) = tag.get_tag(TagKey::TotalTracks) {
                    format!("{}/{}", value, total[0])
                } else {
                    value
                }
            }
            Self::Disc => {
                if let Some(total) = tag.get_tag(TagKey::TotalDiscs) {
                    format!("{}/{}", value, total[0])
                } else {
                    value
                }
            }
            Self::TotalTracks => {
                let single = tag
                    .get_tag(TagKey::Track)
                    .map_or("0".into(), |mut v| v.remove(0));
                format!("{}/{}", single, value)
            }
            Self::TotalDiscs => {
                let single = tag
                    .get_tag(TagKey::Disc)
                    .map_or("0".into(), |mut v| v.remove(0));
                format!("{}/{}", single, value)
            }
            _ => value,
        }
    }

    pub fn file_alter_preservation(&self) -> bool {
        !matches!(
            self,
            Self::ReplayGainAlbumGain
                | Self::ReplayGainAlbumPeak
                | Self::ReplayGainTrackGain
                | Self::ReplayGainTrackPeak
        )
    }
}

fn split_id3_value(value: &str) -> (&str, Option<&str>) {
    if let Some(idx) = value.find('/') {
        let split = value.split_at(idx);
        (split.0, Some(&split.1[1..]))
    } else {
        (value, None)
    }
}

impl TryFrom<metaflac::block::Picture> for Picture {
    type Error = anyhow::Error;

    fn try_from(value: metaflac::block::Picture) -> Result<Self, Self::Error> {
        Ok(Self {
            picture_type: value.picture_type.try_into()?,
            mime_type: value.mime_type.as_str().try_into()?,
            description: value.description,
            data: value.data,
        })
    }
}

impl TryFrom<id3::frame::Picture> for Picture {
    type Error = anyhow::Error;

    fn try_from(value: id3::frame::Picture) -> Result<Self, Self::Error> {
        Ok(Self {
            picture_type: value.picture_type.try_into()?,
            mime_type: value.mime_type.as_str().try_into()?,
            description: value.description,
            data: value.data,
        })
    }
}

impl From<Picture> for metaflac::block::Picture {
    fn from(p: Picture) -> Self {
        let mut picture = metaflac::block::Picture::new();
        picture.picture_type = p.picture_type.into();
        picture.mime_type = p.mime_type.into();
        picture.description = p.description;
        picture.data = p.data;
        picture
    }
}

impl From<Picture> for id3::frame::Picture {
    fn from(p: Picture) -> Self {
        id3::frame::Picture {
            picture_type: p.picture_type.into(),
            mime_type: p.mime_type.into(),
            description: p.description,
            data: p.data,
        }
    }
}

impl TryFrom<&str> for MimeType {
    type Error = anyhow::Error;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        match value {
            "image/bmp" => Ok(Self::Bmp),
            "image/jpeg" | "image/jpg" => Ok(Self::Jpeg),
            "image/png" => Ok(Self::Png),
            _ => Err(anyhow::anyhow!("unsupported image mime type")),
        }
    }
}

impl From<MimeType> for String {
    fn from(t: MimeType) -> Self {
        match t {
            MimeType::Bmp => "image/bmp",
            MimeType::Jpeg => "image/jpeg",
            MimeType::Png => "image/png",
        }
        .into()
    }
}

impl TryFrom<metaflac::block::PictureType> for PictureType {
    type Error = anyhow::Error;

    fn try_from(value: metaflac::block::PictureType) -> Result<Self, Self::Error> {
        match value {
            metaflac::block::PictureType::Artist => Ok(Self::Artist),
            metaflac::block::PictureType::Band => Ok(Self::Band),
            metaflac::block::PictureType::BandLogo => Ok(Self::BandLogo),
            metaflac::block::PictureType::BrightFish => Ok(Self::BrightFish),
            metaflac::block::PictureType::Composer => Ok(Self::Composer),
            metaflac::block::PictureType::Conductor => Ok(Self::Conductor),
            metaflac::block::PictureType::CoverBack => Ok(Self::CoverBack),
            metaflac::block::PictureType::CoverFront => Ok(Self::CoverFront),
            metaflac::block::PictureType::DuringPerformance => Ok(Self::DuringPerformance),
            metaflac::block::PictureType::DuringRecording => Ok(Self::DuringRecording),
            metaflac::block::PictureType::Icon => Ok(Self::Icon),
            metaflac::block::PictureType::Illustration => Ok(Self::Illustration),
            metaflac::block::PictureType::LeadArtist => Ok(Self::LeadArtist),
            metaflac::block::PictureType::Leaflet => Ok(Self::Leaflet),
            metaflac::block::PictureType::Lyricist => Ok(Self::Lyricist),
            metaflac::block::PictureType::Media => Ok(Self::Media),
            metaflac::block::PictureType::Other => Ok(Self::Other),
            metaflac::block::PictureType::OtherIcon => Ok(Self::OtherIcon),
            metaflac::block::PictureType::PublisherLogo => Ok(Self::PublisherLogo),
            metaflac::block::PictureType::RecordingLocation => Ok(Self::RecordingLocation),
            metaflac::block::PictureType::ScreenCapture => Ok(Self::ScreenCapture),
        }
    }
}

impl TryFrom<id3::frame::PictureType> for PictureType {
    type Error = anyhow::Error;

    fn try_from(value: id3::frame::PictureType) -> Result<Self, Self::Error> {
        match value {
            id3::frame::PictureType::Artist => Ok(Self::Artist),
            id3::frame::PictureType::Band => Ok(Self::Band),
            id3::frame::PictureType::BandLogo => Ok(Self::BandLogo),
            id3::frame::PictureType::BrightFish => Ok(Self::BrightFish),
            id3::frame::PictureType::Composer => Ok(Self::Composer),
            id3::frame::PictureType::Conductor => Ok(Self::Conductor),
            id3::frame::PictureType::CoverBack => Ok(Self::CoverBack),
            id3::frame::PictureType::CoverFront => Ok(Self::CoverFront),
            id3::frame::PictureType::DuringPerformance => Ok(Self::DuringPerformance),
            id3::frame::PictureType::DuringRecording => Ok(Self::DuringRecording),
            id3::frame::PictureType::Icon => Ok(Self::Icon),
            id3::frame::PictureType::Illustration => Ok(Self::Illustration),
            id3::frame::PictureType::LeadArtist => Ok(Self::LeadArtist),
            id3::frame::PictureType::Leaflet => Ok(Self::Leaflet),
            id3::frame::PictureType::Lyricist => Ok(Self::Lyricist),
            id3::frame::PictureType::Media => Ok(Self::Media),
            id3::frame::PictureType::Other => Ok(Self::Other),
            id3::frame::PictureType::OtherIcon => Ok(Self::OtherIcon),
            id3::frame::PictureType::PublisherLogo => Ok(Self::PublisherLogo),
            id3::frame::PictureType::RecordingLocation => Ok(Self::RecordingLocation),
            id3::frame::PictureType::ScreenCapture => Ok(Self::ScreenCapture),
            id3::frame::PictureType::Undefined(_) => {
                Err(anyhow::anyhow!("unsupported picture type"))
            }
        }
    }
}

impl From<PictureType> for metaflac::block::PictureType {
    fn from(t: PictureType) -> Self {
        match t {
            PictureType::Artist => metaflac::block::PictureType::Artist,
            PictureType::Band => metaflac::block::PictureType::Band,
            PictureType::BandLogo => metaflac::block::PictureType::BandLogo,
            PictureType::BrightFish => metaflac::block::PictureType::BrightFish,
            PictureType::Composer => metaflac::block::PictureType::Composer,
            PictureType::Conductor => metaflac::block::PictureType::Conductor,
            PictureType::CoverBack => metaflac::block::PictureType::CoverBack,
            PictureType::CoverFront => metaflac::block::PictureType::CoverFront,
            PictureType::DuringPerformance => metaflac::block::PictureType::DuringPerformance,
            PictureType::DuringRecording => metaflac::block::PictureType::DuringRecording,
            PictureType::Icon => metaflac::block::PictureType::Icon,
            PictureType::Illustration => metaflac::block::PictureType::Illustration,
            PictureType::LeadArtist => metaflac::block::PictureType::LeadArtist,
            PictureType::Leaflet => metaflac::block::PictureType::Leaflet,
            PictureType::Lyricist => metaflac::block::PictureType::Lyricist,
            PictureType::Media => metaflac::block::PictureType::Media,
            PictureType::Other => metaflac::block::PictureType::Other,
            PictureType::OtherIcon => metaflac::block::PictureType::OtherIcon,
            PictureType::PublisherLogo => metaflac::block::PictureType::PublisherLogo,
            PictureType::RecordingLocation => metaflac::block::PictureType::RecordingLocation,
            PictureType::ScreenCapture => metaflac::block::PictureType::ScreenCapture,
        }
    }
}

impl From<PictureType> for id3::frame::PictureType {
    fn from(t: PictureType) -> id3::frame::PictureType {
        match t {
            PictureType::Artist => id3::frame::PictureType::Artist,
            PictureType::Band => id3::frame::PictureType::Band,
            PictureType::BandLogo => id3::frame::PictureType::BandLogo,
            PictureType::BrightFish => id3::frame::PictureType::BrightFish,
            PictureType::Composer => id3::frame::PictureType::Composer,
            PictureType::Conductor => id3::frame::PictureType::Conductor,
            PictureType::CoverBack => id3::frame::PictureType::CoverBack,
            PictureType::CoverFront => id3::frame::PictureType::CoverFront,
            PictureType::DuringPerformance => id3::frame::PictureType::DuringPerformance,
            PictureType::DuringRecording => id3::frame::PictureType::DuringRecording,
            PictureType::Icon => id3::frame::PictureType::Icon,
            PictureType::Illustration => id3::frame::PictureType::Illustration,
            PictureType::LeadArtist => id3::frame::PictureType::LeadArtist,
            PictureType::Leaflet => id3::frame::PictureType::Leaflet,
            PictureType::Lyricist => id3::frame::PictureType::Lyricist,
            PictureType::Media => id3::frame::PictureType::Media,
            PictureType::Other => id3::frame::PictureType::Other,
            PictureType::OtherIcon => id3::frame::PictureType::OtherIcon,
            PictureType::PublisherLogo => id3::frame::PictureType::PublisherLogo,
            PictureType::RecordingLocation => id3::frame::PictureType::RecordingLocation,
            PictureType::ScreenCapture => id3::frame::PictureType::ScreenCapture,
        }
    }
}
