pub mod music;
pub mod playlist;
pub mod tag;

use crate::Config;
use anyhow::Result;
use lazy_static::lazy_static;
use relative_path::{RelativePath, RelativePathBuf};
use std::path::{Path, PathBuf};
use std::time::{Duration, SystemTime};

pub use self::music::REPLAYGAIN_TARGET_LUFS;
pub use self::tag::{ALL_PICTURE_TYPES, ALL_TAG_KEYS, NON_COPY_TAG_KEYS};

lazy_static! {
    static ref FILE_TIME: SystemTime = SystemTime::UNIX_EPOCH + Duration::from_secs(1);
}

pub enum FileData<'c> {
    AlbumDirectory(AlbumDirectory<'c>),
    MusicFile(MusicFile<'c>),
    PlaylistFile(PlaylistFile<'c>),
    OtherFile(OtherFile<'c>),
}

pub struct AlbumDirectory<'c> {
    pub file_info: CommonFileInformation<'c>,
    pub music_files: Vec<MusicFile<'c>>,
}

pub struct MusicFile<'c> {
    pub file_info: CommonFileInformation<'c>,
    pub ext: Option<Box<music::MusicFileExt>>,
}

pub struct PlaylistFile<'c> {
    pub file_info: CommonFileInformation<'c>,
    pub ext: Option<playlist::PlaylistFileExt>,
}

pub struct OtherFile<'c> {
    pub file_info: CommonFileInformation<'c>,
    pub file_type: OtherFileType,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum OtherFileType {
    File,
    Dir,
}

#[derive(Debug, Clone)]
pub struct CommonFileInformation<'c> {
    pub rel_path: RelativePathBuf,
    pub src_path: PathBuf,
    pub dst_path: PathBuf,
    pub mtime: SystemTime,
    pub config: &'c Config,
}

pub trait AnyFile {
    fn file_info(&self) -> &CommonFileInformation;

    fn as_other_file<'c>(&self, config: &'c Config) -> OtherFile<'c> {
        let file_info = self.file_info().with_new_config(config);
        OtherFile {
            file_info,
            file_type: if self.file_info().src_path.is_dir() {
                OtherFileType::Dir
            } else {
                OtherFileType::File
            },
        }
    }

    fn should_ignore(&self) -> bool {
        let file_info = self.file_info();
        if file_info.config.force {
            return false;
        }
        if let Some(ignore) = self.should_ignore_special() {
            return ignore;
        }
        if file_info.dst_path.exists() && file_info.mtime <= file_info.config.last_run {
            return true;
        }
        false
    }

    fn should_ignore_special(&self) -> Option<bool> {
        None
    }

    fn populate(&mut self) -> Result<()> {
        Ok(())
    }
}

impl<'c> AnyFile for AlbumDirectory<'c> {
    fn file_info(&self) -> &CommonFileInformation<'c> {
        &self.file_info
    }

    fn should_ignore_special(&self) -> Option<bool> {
        Some(false)
    }
}

impl<'c> AnyFile for MusicFile<'c> {
    fn file_info(&self) -> &CommonFileInformation<'c> {
        &self.file_info
    }

    fn populate(&mut self) -> Result<()> {
        if self.ext.is_none() {
            music::MusicFileExt::populate(self)?;
        }

        Ok(())
    }
}

impl<'c> AnyFile for PlaylistFile<'c> {
    fn file_info(&self) -> &CommonFileInformation<'c> {
        &self.file_info
    }

    fn should_ignore_special(&self) -> Option<bool> {
        if self.file_info.dst_path.exists() {
            let dst_mtime = self
                .ext
                .as_ref()
                .map(|ext| ext.dst_mtime)
                .or_else(|| crate::util::get_mtime(&self.file_info.dst_path).ok())
                .unwrap_or(*FILE_TIME);
            if dst_mtime > self.file_info.config.last_run {
                return Some(false);
            }
        }

        None
    }

    fn populate(&mut self) -> Result<()> {
        if self.ext.is_none() {
            playlist::PlaylistFileExt::populate(self)?;
        }

        Ok(())
    }
}

impl<'c> AnyFile for OtherFile<'c> {
    fn file_info(&self) -> &CommonFileInformation<'c> {
        &self.file_info
    }

    fn should_ignore_special(&self) -> Option<bool> {
        if self.file_type == OtherFileType::Dir && self.file_info.dst_path.exists() {
            return Some(true);
        }
        None
    }
}

impl<'c> FileData<'c> {
    pub fn from_path(path: &Path, config: &'c Config) -> Result<Self> {
        let file_info = CommonFileInformation::from_path(path, config)?;
        Ok(
            if path.is_dir() && file_info.rel_path.components().count() == 2 {
                FileData::AlbumDirectory(AlbumDirectory {
                    file_info,
                    music_files: Vec::new(),
                })
            } else {
                match file_info.rel_path.extension() {
                    Some("flac") | Some("mp3") => FileData::MusicFile(MusicFile {
                        file_info,
                        ext: None,
                    }),
                    Some("m3u") => FileData::PlaylistFile(PlaylistFile {
                        file_info,
                        ext: None,
                    }),
                    Some(_) | None => FileData::OtherFile(OtherFile {
                        file_info,
                        file_type: if path.is_dir() {
                            OtherFileType::Dir
                        } else {
                            OtherFileType::File
                        },
                    }),
                }
            },
        )
    }
}

impl<'c> CommonFileInformation<'c> {
    pub fn from_path(path: &Path, config: &'c Config) -> Result<Self> {
        let rel_path = RelativePath::new(
            path.to_str()
                .ok_or_else(|| anyhow::anyhow!("Path is not a str"))?,
        );
        let rel_path = config.flac_root_rel.relative(rel_path);
        let mut dst_path = rel_path.to_path(&config.mp3_root);
        if rel_path.extension() == Some("flac") {
            dst_path = dst_path.with_extension("mp3")
        }
        let mtime = match crate::util::get_mtime(path) {
            Ok(m) => m,
            Err(e) => {
                log::warn!("Failed to get modification time for {:?}: {}", path, e);
                *FILE_TIME
            }
        };
        Ok(Self {
            src_path: path.to_owned(),
            dst_path,
            rel_path,
            mtime,
            config,
        })
    }

    pub fn with_new_config<'d>(&self, config: &'d Config) -> CommonFileInformation<'d> {
        CommonFileInformation {
            src_path: self.src_path.clone(),
            dst_path: self.dst_path.clone(),
            rel_path: self.rel_path.clone(),
            mtime: self.mtime,
            config,
        }
    }
}
