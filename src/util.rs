use crate::data::AnyFile;
use anyhow::{Context, Result};
use std::path::Path;
use std::time::SystemTime;

const TIMESTAMP_FILE: &str = ".last-playlist-sync";

pub fn copy_permissions<T: AnyFile>(file: &T) -> Result<()> {
    let file = file.file_info();
    let metadata = std::fs::metadata(&file.src_path)
        .with_context(|| format!("Failed to get permission data for {:?}", file.src_path))?;
    std::fs::set_permissions(&file.dst_path, metadata.permissions())
        .with_context(|| format!("Failed to set permission data for {:?}", file.dst_path))?;
    Ok(())
}

pub fn get_last_run_time(path: &Path) -> Result<SystemTime> {
    let mut path = path.to_path_buf();
    path.push(TIMESTAMP_FILE);
    let metadata = std::fs::metadata(path)?;
    Ok(metadata.modified()?)
}

pub fn set_last_run_time(path: &Path) -> Result<()> {
    let mut path = path.to_path_buf();
    path.push(TIMESTAMP_FILE);
    std::fs::File::create(path)?;
    Ok(())
}

pub fn get_mtime(path: &Path) -> Result<SystemTime> {
    let metadata = std::fs::metadata(&path)
        .with_context(|| format!("Failed to get metadata for {:?}", path))?;
    let mtime = metadata
        .modified()
        .with_context(|| format!("Failed to get mtime for {:?}", path))?;
    Ok(mtime)
}
