use crate::data::{
    music::ReplayGainData,
    tag::{Tag, TagKey},
    MusicFile,
};
use anyhow::{Context, Result};
use ebur128::EbuR128;
use std::borrow::Cow;
use std::io::Write;
use symphonia::core::audio::{AudioBufferRef, Signal};
use symphonia::core::codecs::DecoderOptions;
use symphonia::core::errors::Error as SymphoniaError;
use symphonia::core::formats::FormatOptions;
use symphonia::core::io::{MediaSourceStream, MediaSourceStreamOptions};
use symphonia::core::meta::MetadataOptions;
use symphonia::core::probe::Hint;

pub fn transcode_file(file: &mut MusicFile) -> Result<()> {
    let config = file.file_info.config;
    let mut hint = Hint::new();
    let src_extension = file.file_info.rel_path.extension();
    if let Some(ext) = src_extension {
        hint.with_extension(ext);
    }
    let source_file = std::fs::File::open(&file.file_info.src_path)?;
    let source = MediaSourceStream::new(Box::new(source_file), MediaSourceStreamOptions::default());
    let fmt_opts = FormatOptions::default();
    let meta_opts = MetadataOptions::default();
    let decoder_opts = DecoderOptions::default();
    let mut probe = symphonia::default::get_probe()
        .format(&hint, source, &fmt_opts, &meta_opts)
        .with_context(|| format!("Unsupported file format: {:?}", &file.file_info.src_path))?;
    let reader = &mut probe.format;
    let stream = reader.default_stream().unwrap();
    let stream_id = stream.id;
    let mut decoder = symphonia::default::get_codecs()
        .make(&stream.codec_params, &decoder_opts)
        .with_context(|| format!("Failed to open decoder for {:?}", &file.file_info.src_path))?;

    let mut writer =
        lame::Lame::new().ok_or_else(|| anyhow::anyhow!("Failed to open lame encoder"))?;
    let mut output = std::fs::File::create(&file.file_info.dst_path)?;

    let cp = &stream.codec_params;
    if cp.channels.is_none() {
        anyhow::bail!("Missing header parameters: channels");
    }
    if cp.sample_rate.is_none() {
        anyhow::bail!("Missing header parameters: sample rate");
    }
    if cp.channels.unwrap().count() != 2 {
        anyhow::bail!("For now only music files with 2 channels are supported");
    }
    // normalize f32 frames from decoder to i16 frames for encoder (1 sign bit + 15 significant bits)
    let bit_depth_normalize = (1_u64 << 15) as f32;
    if let Err(e) = writer.set_channels(cp.channels.unwrap().count() as u8) {
        anyhow::bail!("Failed to set encoder channels: {:?}", e);
    }
    if let Err(e) = writer.set_sample_rate(cp.sample_rate.unwrap()) {
        anyhow::bail!("Failed to set encoder sample rate: {:?}", e);
    }
    if let Err(e) = writer.set_kilobitrate(config.target_bitrate) {
        anyhow::bail!("Failed to set encoder output bitrate: {:?}", e);
    }
    if let Err(e) = writer.init_params() {
        anyhow::bail!("Failed to set encoder interal params: {:?}", e);
    }

    let calc_rg = config.replaygain != crate::config::ReplayGainMode::None;
    let mut r128 = if calc_rg {
        Some(
            EbuR128::new(
                cp.channels.unwrap().count() as u32,
                cp.sample_rate.unwrap(),
                ebur128::Mode::I | ebur128::Mode::SAMPLE_PEAK,
            )
            .context("Failed initialize loudness calculation")?,
        )
    } else {
        None
    };

    let mut right_buf = Vec::<i16>::with_capacity(0);
    let mut left_buf = Vec::<i16>::with_capacity(0);
    let mut mp3_buf = Vec::<u8>::with_capacity(0);
    let mut r128_buf = Vec::<f32>::with_capacity(0);
    loop {
        let packet = match reader.next_packet() {
            Ok(p) => p,
            Err(SymphoniaError::IoError(io_err))
                if io_err.kind() == std::io::ErrorKind::UnexpectedEof =>
            {
                break;
            }
            Err(e) => {
                return Err(e).context("Failed to read input file");
            }
        };
        if packet.stream_id() != stream_id {
            continue;
        }
        let audio_buffer = match decoder.decode(&packet) {
            Ok(a) => a,
            Err(SymphoniaError::DecodeError(e)) => {
                log::warn!("Decoder error: {}", e);
                continue;
            }
            Err(e) => {
                return Err(e).context("Failed to decode input file");
            }
        };
        let buf = match audio_buffer {
            AudioBufferRef::F32(buf) => buf,
            AudioBufferRef::S32(buf) => {
                let mut f32_buf = buf.make_equivalent();
                f32_buf.render_reserved(Some(buf.frames()));
                buf.convert(&mut f32_buf);
                Cow::Owned(f32_buf)
            }
        };

        let bs = buf.capacity();
        if left_buf.len() < bs {
            left_buf.reserve(bs - left_buf.len());
            right_buf.reserve(bs - right_buf.len());
        }
        left_buf.truncate(0);
        right_buf.truncate(0);
        if calc_rg {
            if r128_buf.len() < bs * 2 {
                r128_buf.reserve(bs * 2 - r128_buf.len());
            }
            r128_buf.truncate(0);
        }

        let left_samples = buf.chan(0);
        let right_samples = buf.chan(1);

        for (left, right) in left_samples.iter().zip(right_samples.iter()) {
            left_buf.push((left * bit_depth_normalize) as i16);
            right_buf.push((right * bit_depth_normalize) as i16);
            if calc_rg {
                r128_buf.push(*left);
                r128_buf.push(*right);
            }
        }

        if let Some(r128) = &mut r128 {
            r128.add_frames_f32(&r128_buf)
                .context("Failed to add samples to loudness calculation")?;
        }

        // taken from lame source code, lame_encode_buffer_sample_t
        // mp3buffer_size in bytes = 1.25*num_samples + 7200
        mp3_buf.resize((1.25 * bs as f32) as usize + 7200, 0);

        let bytes_written = match writer.encode(&left_buf[..], &right_buf[..], &mut mp3_buf[..]) {
            Ok(b) => b,
            Err(e) => {
                anyhow::bail!("Failed to encode audio data: {:?}", e);
            }
        };
        output.write_all(&mp3_buf[0..bytes_written])?;
    }
    if let Some(r128) = r128 {
        let rg = ReplayGainData::from_r128(r128)?;
        log::debug!(
            "Calculated ReplayGain data: loudness: {} LUFS, peak: {}",
            rg.loudness,
            rg.peak
        );
        let music_ext = file.ext.as_mut().expect("BUG: music file unpopulated");
        music_ext
            .source_tag
            .set_tag(TagKey::ReplayGainTrackGain, vec![rg.as_gain()]);
        music_ext
            .source_tag
            .set_tag(TagKey::ReplayGainTrackPeak, vec![rg.as_peak()]);
        music_ext.source_rg.replace(rg);
    }
    Ok(())
}

pub fn calculate_destination_r128(file: &mut MusicFile) -> Result<()> {
    let config = file.file_info.config;
    let calc_rg = config.replaygain != crate::config::ReplayGainMode::None;
    if !calc_rg {
        return Ok(());
    }

    let mut hint = Hint::new();
    let src_extension = file
        .file_info
        .dst_path
        .extension()
        .map(|s| s.to_str())
        .flatten();
    if let Some(ext) = src_extension {
        hint.with_extension(ext);
    }
    let source_file = std::fs::File::open(&file.file_info.dst_path)?;
    let source = MediaSourceStream::new(Box::new(source_file), MediaSourceStreamOptions::default());
    let fmt_opts = FormatOptions::default();
    let meta_opts = MetadataOptions::default();
    let decoder_opts = DecoderOptions::default();
    let mut probe = symphonia::default::get_probe()
        .format(&hint, source, &fmt_opts, &meta_opts)
        .with_context(|| format!("Unsupported file format: {:?}", &file.file_info.dst_path))?;
    let reader = &mut probe.format;
    let stream = reader.default_stream().unwrap();
    let stream_id = stream.id;
    let mut decoder = symphonia::default::get_codecs()
        .make(&stream.codec_params, &decoder_opts)
        .with_context(|| format!("Failed to open decoder for {:?}", &file.file_info.src_path))?;

    let cp = &stream.codec_params;
    if cp.channels.is_none() {
        anyhow::bail!("Missing header parameters: channels");
    }
    if cp.sample_rate.is_none() {
        anyhow::bail!("Missing header parameters: sample rate");
    }
    if cp.channels.unwrap().count() != 2 {
        anyhow::bail!("For now only music files with 2 channels are supported");
    }

    let mut r128 = EbuR128::new(
        cp.channels.unwrap().count() as u32,
        cp.sample_rate.unwrap(),
        ebur128::Mode::I | ebur128::Mode::SAMPLE_PEAK,
    )
    .context("Failed initialize loudness calculation")?;

    let mut r128_buf = Vec::<f32>::with_capacity(0);
    loop {
        let packet = match reader.next_packet() {
            Ok(p) => p,
            Err(SymphoniaError::IoError(io_err))
                if io_err.kind() == std::io::ErrorKind::UnexpectedEof =>
            {
                break;
            }
            Err(e) => {
                return Err(e).context("Failed to read input file");
            }
        };
        if packet.stream_id() != stream_id {
            continue;
        }
        let audio_buffer = match decoder.decode(&packet) {
            Ok(a) => a,
            Err(SymphoniaError::DecodeError(e)) => {
                log::warn!("Decoder error: {}", e);
                continue;
            }
            Err(e) => {
                return Err(e).context("Failed to decode input file");
            }
        };
        let buf = match audio_buffer {
            AudioBufferRef::F32(buf) => buf,
            AudioBufferRef::S32(buf) => {
                let mut f32_buf = buf.make_equivalent();
                f32_buf.render_reserved(Some(buf.frames()));
                buf.convert(&mut f32_buf);
                Cow::Owned(f32_buf)
            }
        };

        let bs = buf.capacity();
        if r128_buf.len() < bs * 2 {
            r128_buf.reserve(bs * 2 - r128_buf.len());
        }
        r128_buf.truncate(0);

        let left_samples = buf.chan(0);
        let right_samples = buf.chan(1);

        for (left, right) in left_samples.iter().zip(right_samples.iter()) {
            r128_buf.push(*left);
            r128_buf.push(*right);
        }

        r128.add_frames_f32(&r128_buf)
            .context("Failed to add samples to loudness calculation")?;
    }
    let rg = ReplayGainData::from_r128(r128)?;
    log::debug!(
        "Calculated destination ReplayGain data: loudness: {} LUFS, peak: {}",
        rg.loudness,
        rg.peak
    );
    let music_ext = file.ext.as_mut().expect("BUG: music file unpopulated");
    music_ext
        .dest_tag
        .set_tag(TagKey::ReplayGainTrackGain, vec![rg.as_gain()]);
    music_ext
        .dest_tag
        .set_tag(TagKey::ReplayGainTrackPeak, vec![rg.as_peak()]);
    music_ext.dest_rg.replace(rg);
    Ok(())
}

pub fn copy_music_tags(file: &mut MusicFile) -> Result<()> {
    let music_ext = if let Some(ext) = &mut file.ext {
        ext
    } else {
        panic!("BUG: music file {:?} unpopulated", file.file_info.rel_path);
    };
    for tag_key in crate::data::ALL_TAG_KEYS
        .iter()
        .filter(|t| !crate::data::NON_COPY_TAG_KEYS.contains(t))
    {
        if let Some(values) = music_ext.source_tag.get_tag(*tag_key) {
            music_ext.dest_tag.set_tag(*tag_key, values);
        }
    }
    for picture_type in crate::data::ALL_PICTURE_TYPES {
        if let Some(pic) = music_ext.source_tag.get_picture(*picture_type) {
            music_ext.dest_tag.set_picture(pic);
        }
    }
    Ok(())
}
