use crate::data::PlaylistFile;
use crate::Config;
use anyhow::{Context, Result};
use m3u::{Entry, EntryExt};
use std::fs::File;
use std::path::Path;

fn set_entry_extension(entry: &mut Entry, base_path: &Path, extension: &str) -> Result<()> {
    if let Entry::Path(path) = entry {
        let mut full_path = base_path.join(&path);
        if let Some(ext) = full_path.extension() {
            if ext != extension && !full_path.exists() {
                let new_path = full_path.with_extension(extension);
                if new_path.exists() {
                    log::trace!("Changing extension of entry {:?} to {}", path, extension);
                    full_path = new_path;
                }
            }
        }
        *path = full_path.strip_prefix(&base_path)?.to_path_buf();
    }

    Ok(())
}

fn write_file(
    entries: Vec<EntryExt>,
    playlist_path: &Path,
    base_path: &Path,
    entry_extension: &str,
    config: &Config,
) -> Result<()> {
    log::debug!("Writing playlist file {:?}", playlist_path);
    let file = File::create(playlist_path)?;
    let mut writer = m3u::Writer::new_ext(file)?;
    for mut entry in entries {
        set_entry_extension(&mut entry.entry, base_path, entry_extension)?;
        if let Entry::Path(path) = &entry.entry {
            if !config.remove_missing || base_path.join(path).exists() {
                writer.write_entry(&entry).context("Failed writing entry")?;
            }
        } else {
            writer.write_entry(&entry).context("Failed writing entry")?;
        }
    }

    Ok(())
}

pub fn write(file: &PlaylistFile) -> Result<()> {
    log::info!("Writing playlist {:?}", file.file_info.rel_path);

    let ext = file.ext.as_ref().expect("BUG: playlist was not populated");

    write_file(
        ext.entries.clone(),
        &file.file_info.src_path,
        &file.file_info.config.flac_root,
        "flac",
        file.file_info.config,
    )?;
    write_file(
        ext.entries.clone(),
        &file.file_info.dst_path,
        &file.file_info.config.mp3_root,
        "mp3",
        file.file_info.config,
    )?;

    Ok(())
}
