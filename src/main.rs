use std::collections::{HashMap, HashSet};
use std::time::SystemTime;

use anyhow::Result;
use chrono::TimeZone;

use crate::data::AnyFile;

use self::config::Config;
use self::data::FileData;

mod config;
mod data;
mod processing;
mod util;

fn main() {
    let crate_setup = config::get_crate_setup();
    let matches = config::get_app(&crate_setup).get_matches();
    // default to 1 (INFO), allow increasing and decreasing
    let verbosity = 1 + matches.occurrences_of("v") as i8 - matches.occurrences_of("q") as i8;
    crate_setup
        .logging_setup()
        .with_verbosity(verbosity)
        .with_filter(|meta| !meta.target().starts_with("symphonia"))
        .build()
        .expect("Failed to setup output!");

    let config = Config::read_from_arg_matches(&matches);
    let dt = config
        .last_run
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap();
    let dt = chrono::Utc.timestamp(0, 0).with_timezone(&chrono::Local)
        + chrono::Duration::from_std(dt).unwrap();

    log::info!("Last run: {}", dt);

    if let Err(e) = main_result(config) {
        log::error!("{:#}", e);
    }
}

fn main_result(config: Config) -> Result<()> {
    if walk_files(&config)? {
        self::util::set_last_run_time(&config.base_path)?;
    }
    log::info!("Processing finished");
    Ok(())
}

pub fn walk_files(config: &Config) -> Result<bool> {
    log::info!("Starting library walk");
    let mut count = 0;
    let mut album_dirs = HashMap::new();
    let mut playlists = Vec::new();
    let mut other_files = Vec::new();
    let mut all_dst_files = HashSet::new();
    for entry in walkdir::WalkDir::new(&config.flac_root)
        .into_iter()
        .filter_map(|d| d.ok())
    {
        let file = FileData::from_path(entry.path(), config);
        match file {
            Ok(f) => {
                count += 1;
                match f {
                    FileData::AlbumDirectory(album_dir) => {
                        all_dst_files.insert(album_dir.file_info().dst_path.clone());
                        other_files.push(album_dir.as_other_file(config));
                        let prev_album = album_dirs
                            .insert(album_dir.file_info.rel_path.as_str().to_owned(), album_dir);
                        assert!(prev_album.is_none(), "BUG: album dir found twice");
                    }
                    FileData::PlaylistFile(playlist_file) => {
                        all_dst_files.insert(playlist_file.file_info().dst_path.clone());
                        playlists.push(playlist_file);
                    }
                    FileData::MusicFile(music_file) => {
                        all_dst_files.insert(music_file.file_info().dst_path.clone());
                        let album_dir = music_file.file_info.rel_path.components().take(2).fold(
                            String::new(),
                            |a, b| {
                                if a.is_empty() {
                                    b.as_str().into()
                                } else {
                                    a + "/" + b.as_str()
                                }
                            },
                        );
                        let album_dir = if let Some(album_dir) = album_dirs.get_mut(&album_dir) {
                            album_dir
                        } else {
                            panic!(
                                "BUG: no album dir found for {:?}",
                                music_file.file_info.rel_path
                            )
                        };
                        album_dir.music_files.push(music_file);
                    }
                    FileData::OtherFile(other_file) => {
                        all_dst_files.insert(other_file.file_info().dst_path.clone());
                        other_files.push(other_file);
                    }
                }
            }
            Err(e) => {
                log::warn!("Can't process file {:?}: {}", entry.path(), e);
            }
        }
    }

    log::info!(
        "Walked {} files, found {} album directories and {} playlists",
        count,
        album_dirs.len(),
        playlists.len(),
    );

    if config.remove_destination_files {
        let mut dst_count = 0;
        log::debug!("Cleaning up unknown destination files");
        for entry in walkdir::WalkDir::new(&config.mp3_root)
            .contents_first(true)
            .into_iter()
            .filter_map(|d| d.ok())
        {
            let path = entry.path();
            if !all_dst_files.contains(path) {
                log::info!("Cleaning up unknown path {:?}", path);
                dst_count += 1;
                let result = if path.is_dir() {
                    std::fs::remove_dir(path)
                } else {
                    std::fs::remove_file(path)
                };

                if let Err(e) = result {
                    log::warn!("error cleaning up destination file {:?}: {}", path, e);
                }
            }
        }

        log::info!("Deleted {} unknown paths in destination", dst_count);
    }

    let mut album_dirs: Vec<_> = album_dirs.into_iter().map(|v| v.1).collect();
    album_dirs.sort_by(|a, b| {
        a.file_info
            .rel_path
            .as_str()
            .cmp(b.file_info.rel_path.as_str())
    });
    other_files.sort_by(|a, b| {
        a.file_info
            .rel_path
            .as_str()
            .cmp(b.file_info.rel_path.as_str())
    });
    playlists.sort_by(|a, b| {
        a.file_info
            .rel_path
            .as_str()
            .cmp(b.file_info.rel_path.as_str())
    });

    let mut success = true;

    // first, copy directory structure and other misc. files
    for file in other_files {
        if file.should_ignore() {
            log::debug!("Ignoring file {:?}: nothing to do", file.file_info.rel_path);
            continue;
        }
        if let Err(e) = processing::process_other_file(file) {
            log::error!("{:#}", e);
            success = false;
        }
    }

    // next, check album directories
    for file in album_dirs {
        if file.should_ignore() {
            log::debug!(
                "Ignoring album {:?}: nothing to do",
                file.file_info.rel_path
            );
            continue;
        }
        let path = file.file_info.rel_path.clone();
        if let Err(e) = processing::process_album_dir(file) {
            log::error!("Error checking album {:?}: {:#}", path, e);
            success = false;
        }
    }

    for file in playlists {
        if file.should_ignore() {
            log::debug!(
                "Ignoring playlist {:?}: nothing to do",
                file.file_info.rel_path
            );
            continue;
        }
        let path = file.file_info.rel_path.clone();
        if let Err(e) = processing::process_playlist_file(file) {
            log::error!("Error checking playlist {:?}: {:#}", path, e);
            success = false;
        }
    }

    Ok(success)
}
